<?php
namespace ApiNovumUwv;

use Crud\FormManager;
use Crud\IApiEndpointFilter;

class EndpointFilter implements IApiEndpointFilter
{

    function filter(FormManager $oManager):bool
    {
        if($oManager instanceof \Crud\Role\CrudRoleManager)
        {
            return true;
        }
        if($oManager instanceof \Crud\User\CrudUserManager)
        {
            return true;
        }
/*
        if(!method_exists($oManager, 'getTags'))
        {
            return false;
        }
        $aTags = $oManager->getTags();



        if(!in_array('NovumUwv', $aTags))
        {
            return false;
        }
*/
        return true;
    }
}
