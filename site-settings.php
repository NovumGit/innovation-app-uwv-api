<?php

return [
    'api_version' => 2,
    'config_dir'  => 'novum.uwv',
    'namespace'   => 'ApiNovumUwv',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'api.uwv.demo.novum.nu',
    'test_domain' => 'api.test.uwv.demo.novum.nu',
    'dev_domain'  => 'api.uwv.innovatieapp.nl',
];
